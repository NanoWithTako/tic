#pragma once
#ifndef TIC_C2_OBJECT_NOTE_H
#define TIC_C2_OBJECT_NOTE_H

namespace tic
{
    /**
     * @brief Do add two number
     *
     * declaration

     * @param[in] l
     * @param[in] r
     * @return int l+r
     */
    int add(int l, int r);

    /// @brief 一个可以带任意参数（任意数目，任意类型）的函数
    int void_param_in_c();

    /// @brief 不带参数的函数
    int void_param_in_cpp();

} // namespace tic

#endif // TIC_C2_OBJECT_NOTE_H
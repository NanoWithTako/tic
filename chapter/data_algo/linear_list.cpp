// #define DLLEXPORT
#include "linear_list.h"

#include <ostream>

const char* get_linear_list_version() {
    return "linear_list version v0.1.0";
}

// namespace tic
// {
//     template <typename T>
//     SeqList<T>::SeqList(int length)
//     {
//         m_data    = new T[length]; // 为一维数组动态分配内存
//         m_length  = 0;
//         m_maxsize = length;
//     }

//     template <typename T>
//     SeqList<T>::~SeqList()
//     {
//         delete[] m_data;
//         m_length = 0; // 非必须操作
//     }

//     template <typename T>
//     bool SeqList<T>::ListInsert(int i, const T& e)
//     {
//         // 如果顺序表已经存满了数据，则不允许再插入新数据了
//         if (m_length >= m_maxsize)
//         {
//             // std::cout << "顺序表已满，不能再进行插入操作了！" << std::endl;
//             // return false;
//             IncreaseSize();
//         }

//         // 判断插入位置i是否合法，i的合法值应该是1到m_length+1之间
//         if (i < 1 || i > (m_length + 1))
//         {
//             std::cout << "元素" << e << "插入的位置" << i
//                       << "不合法, 合法的位置是1到" << m_length + 1 << "之间!"
//                       << std::endl;
//             return false;
//         }

//         // 从最后一个元素开始向前遍历到要插入新元素的第i个位置，分别将这些位置中原来的元素
//         // 向后移动一个位置
//         for (int j = m_length; j >= i; --j)
//         {
//             m_data[j] = m_data[j - 1];
//         }
//         // 在指定位置i处插入元素e，因为数组下标从0开始，所以这里用i-1表示插入位置所对应的数组下标
//         m_data[i - 1] = e;
//         std::cout << "成功在位置为" << i << "处插入元素" << m_data[i - 1] << "!"
//                   << std::endl;
//         m_length++;
//         return true;
//     }

//     template <typename T>
//     bool SeqList<T>::ListDelete(int i)
//     {
//         if (m_length < 1)
//         {
//             std::cout << "当前顺序表为空, 不能删除任何数据!" << std::endl;
//             return false;
//         }
//         if (i < 1 || i > m_length)
//         {
//             std::cout << "删除的位置" << i << "不合法, 合法的位置是1到"
//                       << m_length << "之间!" << std::endl;
//             return false;
//         }
//         std::cout << "成功删除位置为" << i << "的元素，该元素的值为"
//                   << m_data[i - 1] << "!" << std::endl;
//         // 从数组中第i+1个位置开始向后遍历所有元素，分别将这些位置中原有的元素向前移动一个位置
//         for (int j = i; j < m_length; ++j)
//         {
//             m_data[j - 1] = m_data[j];
//         }
//         m_length--; // 实际表长-1
//         return true;
//     }

//     template <typename T>
//     bool SeqList<T>::GetElem(int i, T& e)
//     {
//         if (m_length < 1)
//         {
//             std::cout << "当前顺序表为空, 不能获取任何数据!" << std::endl;
//             return false;
//         }

//         if (i < 1 || i > m_length)
//         {
//             std::cout << "获取元素的位置" << i << "不合法, 合法的位置是1到"
//                       << m_length << "之间!" << std::endl;
//             return false;
//         }
//         e = m_data[i - 1];
//         std::cout << "成功获取位置为" << i << "的元素, 该元素的值为"
//                   << m_data[i - 1] << "!" << std::endl;
//         return true;
//     }

//     // 按照元素值查找其在顺序表中第一次出现的位置
//     template <typename T>
//     int SeqList<T>::LocateElem(const T& e)
//     {
//         for (int i = 0; i < m_length; ++i)
//         {
//             if (m_data[i] == e)
//             {
//                 std::cout << "值为" << e << "的元素在顺序表中第一次出现的位置为"
//                           << i + 1 << "!" << std::endl;
//                 return i + 1;
//             }
//         }

//         std::cout << "值为" << e << "的元素在顺序表中没有找到!" << std::endl;
//         return -1;
//     }

//     template <typename T>
//     void SeqList<T>::DispList()
//     {
//         for (int i = 0; i < m_length; ++i)
//         {
//             std::cout << m_data[i] << " ";
//         }
//         std::cout << std::endl;
//     }

//     template <typename T>
//     int SeqList<T>::ListLength()
//     {
//         return m_length;
//     }

//     // 翻转顺序表，时间复杂度为O(n)
//     template <typename T>
//     void SeqList<T>::ReverseList()
//     {
//         if (m_length <= 1)
//         {
//             // 如果顺序表中没有元素或者只有一个元素，那么就不用做任何操作
//             return;
//         }
//         T temp;
//         for (int i = 0; i < m_length / 2; ++i)
//         {
//             temp                     = m_data[i];
//             m_data[i]                = m_data[m_length - i - 1];
//             m_data[m_length - i - 1] = temp;
//         }
//     }

//     // 当顺序表存满数据后可以调用此函数为顺序表扩容，时间复杂度为O(n)
//     template <class T>
//     void SeqList<T>::IncreaseSize()
//     {
//         T* p = m_data;
//         m_data = new T[m_maxsize + kIncSize]; // 重新为顺序表分配更大的内存空间
//         for (int i = 0; i < m_length; i++)
//         {
//             m_data[i] = p[i]; // 将数据复制到新区域
//         }
//         m_maxsize = m_maxsize + kIncSize; // 顺序表最大长度增加IncSize
//         delete[] p;                       // 释放原来的内存空间
//     }
// } // namespace tic
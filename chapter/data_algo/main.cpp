#include <Windows.h>

#include "linear_list.h"

int main()
{
    // 设置控制台输出为UTF-8编码格式
    SetConsoleOutputCP(CP_UTF8);

    std::cout << get_linear_list_version() << std::endl;

    tic::SeqList<int> seqobj(10);

    seqobj.ListInsert(1, 15);
    seqobj.ListInsert(2, 10);
    seqobj.ListInsert(30, 8);

    seqobj.ListDelete(1);

    int eval = 0;
    seqobj.GetElem(1,
                   eval); // 如果GetElem()返回true, 则eval中保存着获取到的元素

    int findvalue = 10;
    seqobj.LocateElem(findvalue);

    seqobj.ListInsert(2, 100);
    seqobj.DispList();
    std::cout << seqobj.ListLength() << std::endl;
    seqobj.ReverseList();
    seqobj.DispList();


    for (int i = 3; i < 30; ++i)
    {
        seqobj.ListInsert(i, i * 2);
    }
    seqobj.DispList();
    return 0;
}
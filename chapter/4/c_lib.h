#pragma once
#ifndef TIC_C4_CLIB_H
#    define TIC_C4_CLIB_H

#    ifdef DLLEXPORT
#        define CLIB_API __declspec(dllexport)
#    else
#        define CLIB_API /* __declspec(dllimport) */
#    endif

#    ifdef __cplusplus
extern "C"
{
#    endif // __cplusplus

    /**
     * @brief
     *
     */
    typedef struct CStashTag
    {
        int size;     // Size of each space
        int quantity; // Number of storage spaces
        int next;     // Next empty space
        // Dynamically allocated array of bytes:
        unsigned char* storage;
    } CStash;

    /*
    Microsoft (R) COFF/PE Dumper Version 14.29.30148.0
    Copyright (C) Microsoft Corporation.  All rights reserved.


    Dump of file c_lib.lib

    File Type: LIBRARY

         Exports

           ordinal    name

                      add
                      cleanup
                      count
                      fetch
                      inflate
                      initialize

      Summary

              14 .idata$2
              14 .idata$3
               8 .idata$4
               8 .idata$5
               A .idata$6

    未使用c导出时：
    Microsoft (R) COFF/PE Dumper Version 14.29.30148.0
    Copyright (C) Microsoft Corporation.  All rights reserved.


    Dump of file c_lib.lib

    File Type: LIBRARY

         Exports

           ordinal    name

                      ?add@@YAHPEAUCStashTag@@PEBX@Z (int __cdecl add(struct
    CStashTag *,void const *)) ?cleanup@@YAXPEAUCStashTag@@@Z (void __cdecl
    cleanup(struct CStashTag *)) ?count@@YAHPEAUCStashTag@@@Z (int __cdecl
    count(struct CStashTag *)) ?fetch@@YAPEAXPEAUCStashTag@@H@Z (void * __cdecl
    fetch(struct CStashTag *,int)) ?inflate@@YAXPEAUCStashTag@@H@Z (void __cdecl
    inflate(struct CStashTag *,int)) ?initialize@@YAXPEAUCStashTag@@H@Z (void
    __cdecl initialize(struct CStashTag *,int))

      Summary

              14 .idata$2
              14 .idata$3
               8 .idata$4
               8 .idata$5
               A .idata$6
    */

    /**
     * @brief
     *
     * @param s
     * @param size
     * @return CLIB_API
     */
    CLIB_API void initialize(CStash* s, int size);

    /**
     * @brief
     *
     * @param s
     * @return CLIB_API
     */
    CLIB_API void cleanup(CStash* s);

    /**
     * @brief
     *
     * @param s
     * @param element
     * @return CLIB_API
     */
    CLIB_API int add(CStash* s, const void* element);

    /**
     * @brief
     *
     * @param s
     * @param index
     * @return CLIB_API*
     */
    CLIB_API void* fetch(CStash* s, int index);

    /**
     * @brief
     *
     * @param s
     * @return CLIB_API
     */
    CLIB_API int count(CStash* s);

    /**
     * @brief
     *
     * @param s
     * @param increase
     * @return CLIB_API
     */
    CLIB_API void inflate(CStash* s, int increase);

#    ifdef __cplusplus
}
#    endif // __cplusplus

#endif // TIC_C4_CLIB_H
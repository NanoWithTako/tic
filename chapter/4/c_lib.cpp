#define DLLEXPORT
#include "c_lib.h"

#include <cassert>
#include <cstdio>
#include <iostream>

using namespace std;

const int increment = 100;

void initialize(CStash *s, int size)
{
    s->size = size;
    s->quantity = 0;
    s->storage = 0;
    s->next = 0;
}

int add(CStash *s, const void *element)
{
    if (s->next >= s->quantity) // Enough space left?
        inflate(s, increment);
    // Copy element into storage
    // starting at next empty space:
    int start_bytes = s->next * s->size;
    unsigned char *e = (unsigned char *)element;
    for (int i = 0; i < s->size; ++i)
    {
        s->storage[start_bytes + i] = e[i];
    }
    s->next++;
    return (s->next - 1); // Index number
}

void *fetch(CStash *s, int index)
{
    // Check index boundaries:
    assert(0 <= index);
    if (index >= s->next)
    {
        return 0; // To indicate the end
    }
    return &(s->storage[index * s->size]);
}

int count(CStash *s)
{
    return s->next; // Element in CStash
}

void inflate(CStash *s, int increase)
{
    assert(increase > 0);
    int new_quantity = s->quantity + increase;
    int new_bytes = new_quantity * s->size;
    int old_bytes = s->quantity * s->size;
    unsigned char *b = new unsigned char[new_bytes];
    for (int i = 0; i < old_bytes; ++i)
    {
        b[i] = s->storage[i]; // Copy old to new
    }
    delete[] (s->storage);
    s->storage = b;
    s->quantity = new_quantity;
}

void cleanup(CStash *s)
{
    if (s->storage != 0)
    {
        printf("freeing storage\n");
        delete[] s->storage;
    }
}
#include "light.h"

namespace tic
{

    Light::Light() {}

    Light::~Light() {}

    void Light::on() {}

    void Light::off() {}

    void Light::brighten() {}

    void Light::dim() {}

} // namespace tic

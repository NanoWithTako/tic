#pragma once
#ifndef TIC_C1_LIGHT_H
#define TIC_C1_LIGHT_H

namespace tic
{

    class Light final
    {
    public:
        Light();
        ~Light();

        void on();
        void off();
        void brighten();
        void dim();
    };

} // namespace tic

#endif // TIC_C1_LIGHT_H
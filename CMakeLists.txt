cmake_minimum_required(VERSION 3.14..)
project(thinking_in_cpp)

add_subdirectory(${CMAKE_SOURCE_DIR}/chapter/1)
add_subdirectory(${CMAKE_SOURCE_DIR}/chapter/2)
add_subdirectory(${CMAKE_SOURCE_DIR}/chapter/4)
add_subdirectory(${CMAKE_SOURCE_DIR}/chapter/data_algo)